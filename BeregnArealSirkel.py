# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 14:38:09 2019

@author: rouhani
"""

"""
def <navn>(parametere):
    <innhold>
    <return>
"""

def beregnArealSirkel(r):
    return 3.1428*r*r

r=10
a=beregnArealSirkel(r)

print("Areal av sirkel med radius "+str(r) +" er "+str(a))

